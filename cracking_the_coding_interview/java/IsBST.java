    // https://www.hackerrank.com/challenges/ctci-is-binary-search-tree/problem

    boolean checkSubtree(Node root, int min, int max) {
        if (root == null) {
            return true;
        }
        
        if (root.data > min && root.data < max) {
            return checkSubtree(root.left, min, root.data) && checkSubtree(root.right, root.data, max);
        }
        
        return false;
    }

    boolean checkBST(Node root) {
        if (root == null) {
            return true;
        }
        
        return checkSubtree(root.left, Integer.MIN_VALUE ,root.data) &&
            checkSubtree(root.right, root.data ,Integer.MAX_VALUE);
    }