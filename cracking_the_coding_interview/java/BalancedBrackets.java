// https://www.hackerrank.com/challenges/ctci-balanced-brackets/problem

    public static boolean isBalanced(String expression) {
        Stack<Character> stack = new Stack<>();
        
        for (int i = 0; i < expression.length(); i++) {
            char c = expression.charAt(i);
            
            if (c == '{' || c == '[' || c == '(') {
                stack.push(c);
            } else {
                if (stack.size() == 0) {
                    return false;
                } else if (c == '}') {
                    if (stack.pop() != '{') {
                        return false;
                    }
                } else if (c == ']') {
                    if (stack.pop() != '[') {
                        return false;
                    }
                } else if (c == ')') {
                    if (stack.pop() != '(') {
                        return false;
                    }
                }
            }
        }
        
        if (stack.size() != 0) {
            return false;
        }
        
        return true;
    }