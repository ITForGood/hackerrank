import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

// https://www.hackerrank.com/challenges/ctci-ransom-note/problem

public class CtciRansomNote {

    public static String solve(String[] magazine, String[] ransom) {

        HashMap<String, Integer> hm = new HashMap<>();

        for (int i = 0; i < magazine.length; i++) {
            if (hm.containsKey(magazine[i])) {
                hm.put(magazine[i], hm.get(magazine[i]) + 1);
            } else {
                hm.put(magazine[i], 1);
            }
        }

        for (int i = 0; i < ransom.length; i++) {
            if (hm.containsKey(ransom[i]) == false) {
                return "No";
            } else {
                hm.put(ransom[i], hm.get(ransom[i]) - 1);
                if (hm.get(ransom[i]) < 0) {
                    return "No";
                }
            }
        }

        return "Yes";
    }

    public static void main(String[] args) {
        String[] magazine = "a b c d".split(" ");
        String[] ransom = "a b c".split(" ")

        System.out.println(solve(magazine, ransom));
    }
}
