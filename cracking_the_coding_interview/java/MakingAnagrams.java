import java.util.HashMap;
import java.util.Set;

// https://gitlab.com/ITForGood/hackerrank

public class MakingAnagrams {

    public static int test(String first, String second) {

        HashMap<Character, Integer> hm = new HashMap<>();

        for (int i = 0; i < first.length(); i++) {
            if (hm.containsKey(first.charAt(i))) {
                hm.put(first.charAt(i), hm.get(first.charAt(i)) + 1);
            } else {
                hm.put(first.charAt(i), 1);
            }
        }

        for (int i = 0; i < second.length(); i++) {
            if (hm.containsKey(second.charAt(i))) {
                hm.put(second.charAt(i), hm.get(second.charAt(i)) - 1);
            } else {
                hm.put(second.charAt(i), -1);
            }
        }

        Set<Character> set = hm.keySet();

        int counter = 0;
        for (Character c : set) {
            counter += Math.abs(hm.get(c));
        }

        return counter;
    }

    public static void main(String[] args) {
        System.out.println(test("a", ""));
    }
}
