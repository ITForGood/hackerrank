#!/usr/bin/env python
#Date: 2018.03.20
#Author: Donghun Lee (donghun.lee7@gmail.com)
#https://www.hackerrank.com/challenges/ctci-ransom-note/problem

def ransom_note(magazine, ransom):
    if len(magazine) == len(ransom):
        if set(ransom) == set(magazine):
            return True
    for i in ransom:
        try:
            magazine.pop(magazine.index(i))
        except:
            return False
    return True

m, n = map(int, raw_input().strip().split(' '))
magazine = raw_input().strip().split(' ')
ransom = raw_input().strip().split(' ')
answer = ransom_note(magazine, ransom)
if(answer):
    print "Yes"
else:
    print "No"
