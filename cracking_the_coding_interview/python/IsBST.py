#!/usr/bin/env python
#Date: 2018.03.20
#Author: Donghun Lee (donghun.lee7@gmail.com)
#https://www.hackerrank.com/challenges/ctci-is-binary-search-tree/problem

""" Node is defined as
class node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None
"""
MIN = 0
MAX = 10000

def checkBST(root):
    return checkSubTree(root, MIN, MAX)
    
def checkSubTree(node, minn, maxx):
    if node is None:
        return True
    return minn < node.data and node.data < maxx and \
            checkSubTree(node.left, minn, node.data) and \
            checkSubTree(node.right, node.data, maxx)