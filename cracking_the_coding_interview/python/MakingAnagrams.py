#!/usr/bin/env python
#Date: 2018.03.20
#Author: Donghun Lee (donghun.lee7@gmail.com)
#https://www.hackerrank.com/challenges/ctci-making-anagrams/problem

def number_needed(a, b):
    a_list = [i for i in a]
    b_list = [i for i in b]
    for i in a:
        try:
            b_list.pop(b_list.index(i))
        except:
            pass
    for i in b:
        try:
            a_list.pop(a_list.index(i))
        except:
            pass
    return len(a_list) + len(b_list)

a = raw_input().strip()
b = raw_input().strip()

print number_needed(a, b)

