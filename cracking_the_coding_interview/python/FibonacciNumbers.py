#!/usr/bin/env python
#Date: 2018.03.27
#Author: Donghun Lee (donghun.lee7@gmail.com)
#https://www.hackerrank.com/challenges/ctci-fibonacci-numbers/problem

def fibonacci(n):
    # Write your code here.
    if n > 0:
        if n is 0: return 0
        if n is 1: return 1
        return int(fibonacci(n-1)) + int(fibonacci(n-2))
    return 0
n = int(raw_input())
print(fibonacci(n))

