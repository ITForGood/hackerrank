#!/usr/bin/env python
#Date: 2018.03.25
#Author: Donghun Lee (donghun.lee7@gmail.com)
#https://www.hackerrank.com/challenges/ctci-balanced-brackets/problem

def is_matched(expression):
    if len(expression) % 2 == 1:
        return False
    opened_bracket = []
    for i in range(len(expression)):
        if is_open(expression[i]):
            opened_bracket.append(expression[i])
        else:
            try:
                if expression[i] != opposite_bracket(opened_bracket.pop()):
                    return False
            except:
                return False
    if len(opened_bracket) == 0: return True
    else: return False

def is_open(c):
    if c in ['(', '{', '[']: return True
    else: return False
    
def opposite_bracket(open_bracket):
    if open_bracket is '(':
        return ')'
    if open_bracket is '{':
        return '}'
    if open_bracket is '[':
        return ']'

t = int(raw_input().strip())
for a0 in xrange(t):
    expression = raw_input().strip()
    if is_matched(expression) == True:
        print "YES"
    else:
        print "NO"
